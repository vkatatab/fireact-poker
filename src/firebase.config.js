import firebase from 'firebase';

var config = {
  apiKey: 'AIzaSyBl4kPGLOpkE9iXel-PymQT0ZDHwNNgbfI',
  authDomain: 'fireact-poker.firebaseapp.com',
  databaseURL: 'https://fireact-poker.firebaseio.com',
  projectId: 'fireact-poker',
  storageBucket: 'fireact-poker.appspot.com',
  messagingSenderId: '714139260354'
};

firebase.initializeApp(config);

export default firebase;
