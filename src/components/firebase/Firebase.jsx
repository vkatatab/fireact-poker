import React from 'react';
import firebase from 'firebase.config';
import Button from 'material-ui/IconButton';
import FaFacebook from 'react-icons/lib/fa/facebook';
import styles from './Facebook.scss';

export default class Facebook extends React.Component {

  loginWithFacebook = () => {
    const provider = new firebase.auth.FacebookAuthProvider();
    provider.setCustomParameters({
      'display': 'popup'
    });
    firebase.auth().signInWithPopup(provider).then(function(result) {
      var token = result.credential.accessToken;
      var user = result.user;
    }).catch(function(error) {
      var errorCode = error.code;
      var errorMessage = error.message;
      var email = error.email;
      var credential = error.credential;
    });
  }

  render() {
    return null;
  }
}
