const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
  template: './index.html',
  filename: 'index.html',
  inject: 'body'
})

module.exports = {
  entry: './index.js',
  output: {
    path: path.resolve('dist'),
    filename: 'index_bundle.js'
  },
  resolve: {
    modules: [
      path.resolve('./src'),
      path.resolve('./node_modules')
    ],
    extensions: [
        '.js', '.jsx'
    ],
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },{
          test: /\.scss/,
          use: [{
              loader: "style-loader"
          }, {
              loader: "css-loader",
              options: {
                modules: true,
              }
          }, {
              loader: "sass-loader"
          }]
      }
    ]
  },
  plugins: [HtmlWebpackPluginConfig]
}
